package com.openlife.presenter.contract;

public interface ParkingContract {
    interface View extends BaseView {
        void showDatePickerDialog(int year, int month, int dayOfMonth);
        void showTimePickerDialog(int hourOfDay, int minutes);
        void navigateBack();
        void fillStart(long millis);
        void fillEnd(long millis);
        void showStartError(boolean isError);
        void showEndError(boolean isError);
        void showFioError();
        void showCompanyError();
        void showLabelError();
        void showNumberError();
        void showRegionError();
        void navigateToMyRequests();
    }

    interface Presenter extends BasePresenter {
        void startClick();
        void endClick();
        void send(String fio, String company, String label, String number, String region);
        void timeSet(int hourOfDay, int minute);
        void dateSet(int year, int month, int dayOfMonth);
    }
}
