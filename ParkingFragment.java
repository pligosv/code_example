package com.openlife.view.fragment.services;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TimePicker;

import com.openlife.R;
import com.openlife.core.utils.TimeUtils;
import com.openlife.presenter.contract.ParkingContract;
import com.openlife.presenter.services.ParkingPresenter;
import com.openlife.view.annotation.Layout;
import com.openlife.view.annotation.Presenter;
import com.openlife.view.fragment.AbstractFragment;
import com.openlife.view.fragment.request.RequestTabsFragment;
import com.openlife.view.view.request.RequestEditText;
import com.openlife.view.view.request.RequestPick;

import butterknife.BindView;
import butterknife.OnClick;

@Layout(R.layout.fragment_parking)
@Presenter(ParkingPresenter.class)
public class ParkingFragment extends AbstractFragment<ParkingPresenter>
        implements ParkingContract.View, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    @BindView(R.id.rp_start_date)
    RequestPick rpStart;
    @BindView(R.id.rp_end_date)
    RequestPick rpEnd;
    @BindView(R.id.ret_full_name)
    RequestEditText retFio;
    @BindView(R.id.ret_company)
    RequestEditText retCompany;
    @BindView(R.id.ret_label)
    RequestEditText retLabel;
    @BindView(R.id.ret_number)
    RequestEditText retNumber;
    @BindView(R.id.ret_region)
    RequestEditText retRegion;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.pb_request)
    ProgressBar progressBar;

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.parking_title);
    }

    @Override
    protected int getNavigationIconResId() {
        return R.drawable.ic_navigation_back_services;
    }

    @Override
    public void showDatePickerDialog(int year, int month, int dayOfMonth) {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this,
                year, month, dayOfMonth);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        dialog.show();
    }

    @Override
    public void showTimePickerDialog(int hourOfDay, int minutes) {
        TimePickerDialog dialog = new TimePickerDialog(getActivity(), this,
                hourOfDay, minutes, true);
        dialog.show();
    }

    @Override
    public void navigateBack() {
        getTransitManager().back();
    }

    @Override
    public void fillStart(long millis) {
        rpStart.setValue(TimeUtils.formatRequestDate(millis));
        rpEnd.showError(false);
    }

    @Override
    public void fillEnd(long millis) {
        rpEnd.setValue(TimeUtils.formatRequestDate(millis));
        rpStart.showError(false);
    }

    @Override
    public void showStartError(boolean isError) {
        rpStart.showError(isError);
    }

    @Override
    public void showEndError(boolean isError) {
        rpEnd.showError(isError);
    }

    @Override
    public void showFioError() {
        retFio.showError();
    }

    @Override
    public void showCompanyError() {
        retCompany.showError();
    }

    @Override
    public void showLabelError() {
        retLabel.showError();
    }

    @Override
    public void showNumberError() {
        retNumber.showError();
    }

    @Override
    public void showRegionError() {
        retRegion.showError();
    }

    @Override
    public void setProgressVisibility(boolean isVisible) {
        rpStart.setEnabled(!isVisible);
        rpEnd.setEnabled(!isVisible);
        retFio.setEnabled(!isVisible);
        retCompany.setEnabled(!isVisible);
        retLabel.setEnabled(!isVisible);
        retNumber.setEnabled(!isVisible);
        retRegion.setEnabled(!isVisible);
        btnSend.setEnabled(!isVisible);
        progressBar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        getPresenter().timeSet(hourOfDay, minute);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        getPresenter().dateSet(year, month, dayOfMonth);
    }

    @Override
    public void navigateToMyRequests() {
        Bundle bundle = new Bundle();
        bundle.putInt(RequestTabsFragment.KEY_START_TAB, RequestTabsFragment.MY_REQUEST_TAB_INDEX);
        getTransitManager().switchFragment(RequestTabsFragment.class, bundle);
    }

    @OnClick(R.id.rp_start_date)
    void onStartClick() {
        getPresenter().startClick();
    }

    @OnClick(R.id.rp_end_date)
    void onEndClick() {
        getPresenter().endClick();
    }

    @OnClick(R.id.btn_send)
    void onSendClick() {
        getPresenter().send(retFio.getValue(), retCompany.getValue(), retLabel.getValue(),
                retNumber.getValue(), retRegion.getValue());
    }
}
