package com.openlife.presenter.services;

import com.openlife.R;
import com.openlife.core.utils.ErrorUtils;
import com.openlife.core.utils.TimeUtils;
import com.openlife.model.services.parking.Parking;
import com.openlife.presenter.AbstractPresenter;
import com.openlife.presenter.contract.ParkingContract;

import java.util.Calendar;

public class ParkingPresenter extends AbstractPresenter<ParkingContract.View>
        implements ParkingContract.Presenter {

    private final Calendar startDate = Calendar.getInstance();
    private final Calendar endDate = Calendar.getInstance();

    private boolean dialogForStart = true;

    @Override
    public void viewCreated() {
        super.viewCreated();
        getView().fillStart(startDate.getTimeInMillis());
        getView().fillEnd(endDate.getTimeInMillis());
        getView().setDataLoaded();
    }

    @Override
    public void startClick() {
        dialogForStart = true;
        getView().showDatePickerDialog(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH),
                startDate.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void endClick() {
        dialogForStart = false;
        getView().showDatePickerDialog(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH),
                endDate.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void send(String fio, String company, String label, String number, String region) {
        if (paramsValid(fio, company, label, number, region)) {
            getView().setProgressVisibility(true);
            String startTimeStamp = TimeUtils.formatServiceDateToApi(startDate.getTimeInMillis());
            String endTimeStamp = TimeUtils.formatServiceDateToApi(endDate.getTimeInMillis());

            Parking parking = new Parking();
            parking.setStartTimeStamp(startTimeStamp);
            parking.setEndTimeStamp(endTimeStamp);
            parking.setPersonName(fio);
            parking.setCompany(company);
            parking.setAutoModel(label);
            parking.setAutoNum(number);
            parking.setAutoRegion(region);

            execute(getDataProvider().createParking(parking), result -> {
                getView().setProgressVisibility(false);
                getView().navigateBack();
                getView().navigateToMyRequests();
                getView().showToast(R.string.request_success);
            }, error -> {
                getView().setProgressVisibility(false);
                getView().showToast(ErrorUtils.parseError(error));
            });
        }
    }

    @Override
    public void timeSet(int hourOfDay, int minute) {
        if (dialogForStart) {
            startDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
            startDate.set(Calendar.MINUTE, minute);
            getView().fillStart(startDate.getTimeInMillis());
        } else {
            endDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
            endDate.set(Calendar.MINUTE, minute);
            getView().fillEnd(endDate.getTimeInMillis());
        }
    }

    @Override
    public void dateSet(int year, int month, int dayOfMonth) {
        if (dialogForStart) {
            startDate.set(Calendar.YEAR, year);
            startDate.set(Calendar.MONTH, month);
            startDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            getView().fillStart(startDate.getTimeInMillis());
        } else {
            endDate.set(Calendar.YEAR, year);
            endDate.set(Calendar.MONTH, month);
            endDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            getView().fillEnd(endDate.getTimeInMillis());
        }
        getView().showTimePickerDialog(startDate.get(Calendar.HOUR_OF_DAY), startDate.get(Calendar.MINUTE));
    }


    private boolean paramsValid(String fio, String company, String label, String number, String region) {
        boolean isValid = true;
        if (fio.isEmpty()) {
            getView().showFioError();
            isValid = false;
        }
        if (company.isEmpty()) {
            getView().showCompanyError();
            isValid = false;
        }
        if (label.isEmpty()) {
            getView().showLabelError();
            isValid = false;
        }
        if (number.isEmpty()) {
            getView().showNumberError();
            isValid = false;
        }
        if (region.isEmpty()) {
            getView().showRegionError();
            isValid = false;
        }
        if (endDate.getTimeInMillis() <= startDate.getTimeInMillis()) {
            getView().showStartError(true);
            getView().showEndError(true);
            getView().showToast(R.string.parking_time_error);
            isValid = false;
        }
        return isValid;
    }
}
